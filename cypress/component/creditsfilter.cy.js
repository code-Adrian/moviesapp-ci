import Login from '../../src/components/filterCreditsCard'
const peopleInvolved = ["Crew","Cast"];

describe('Credits page filter card', () => {
  Cypress.on('uncaught:exception', (err, runnable) => {
    return false
    });

    beforeEach(() => {
      cy.mount(<Login/>)
    });

    it('Crew text select test', () => {
      cy.get("#involved-select").click();
      cy.get("li").contains(peopleInvolved[0]).click();
      cy.get("li").contains(peopleInvolved[0]).click();
      cy.get("#involved-select").contains(peopleInvolved[0]);
    });

    it('Casts text select test', () => {
      cy.get("#involved-select").click();
      cy.get("li").contains(peopleInvolved[1]).click();
      cy.get("li").contains(peopleInvolved[1]).click();
      cy.get("#involved-select").contains(peopleInvolved[1]);
    });
});