# Assignment 1 - Agile Software Practices

Name: Adrian Bernacki

## Overview

The following repository contains commits that define the workflow and progression throughout the assignment. No commits were made directly in the main,develop or master branch and all the branches were created from the develop branch and once done they were merged into develop then from develop to main.

## React App Features
+ Login, Registration, Delete User and Signing Out + Private Routes
+ Filtering by ratings for Movies and Tv shows.
+ Credits page to view people involved in Movies or Tv Shows
+ Filtering and searching by the Crew or the Casts in Credits page for Movies or TvShows
+ Popular Movies List with Reviews
+ Now Playing List with Reviews
+ Tv List to view tv shows with Reviews
+ Fully Cached
+ Pagination

## Automated Tests.

### Features
+ Automated E2E Tests. (Navigation, Error/Exception, Nesting, Cypress Custom Commands)
+ Continuous Integration. (Branching Policy)
+ Source control.
+ Bundling/Code splitting.
+ Source control (Pull request).
+ Cypress component testing.

### E2E tests
+ cypress/e2e/review.cy.js
+ cypress/e2e/login.cy.js
+ cypress/e2e/register.cy.js
+ cypress/e2e/ratings.cy.js
+ cypress/e2e/pagination.cy.js
+ cypress/e2e/credits.cy.js

### Best test cases.
+ cypress/e2e/pagination.cy.js
+ cypress/e2e/credits.cy.js

### Cypress Custom commands
+ All E2E test files mentioned above use custom commands to login, log out, and to check page headers using the cypress/support/e2e.js

### Code Splitting
+ src/index.js
+ src/components/templateTvPage/index.js
+ src/components/templateTvListPageindex.js
+ src/components/templatetvCreditsPageindex.js
+ src/components/templateMovieListPageindex.js
+ src/images/code-splitting-proof.png (Proof of Build files)

### Component tests
+ cypress/component/creditsfilter.cy.js

## Pull requests
+ https://github.com/code-Adrian/agile-assignment1
+ src/images/github-fork-proof.png

## Independent Learning
+ cypress/component/creditsfilter.cy.js (Very simple component test)
+ Read documentation on changing the yaml file to run component test seperate to the e2e test.
+ Watched videos to understand component testing.
+ Successful component gitlab test proof: component-proof.png

### Other Independent Learning
+ Took use of cypress/fixtures/login.js used for authentication testing according to the cypress documentation.

## New Branches
+ main
+ develop
+ master
+ assignment-prep
+ fixtures
+ custom-commands
+ support-functions
+ e2e-login
+ e2e-register
+ e2e-pagination
+ e2e-ratings
+ e2e-review
+ e2e-credits
+ e2e-pagination-fix
+ e2e-pagination-fix2
+ code-bundling-routes
+ code-splitting
+ assignment-fork-demo
+ pull-req-1
+ component-filter-select-test

## Branch Descriptions
+ main - The main branch contains all merges from the develop branch. This branch was only used for pushing to gitlab and merging once into the master branch.

+ develop - The develop branch contains all merges from emanating branches. All additional branches that involved e2e / code bundling and component testing were created from the develop branch. This branch was also used to push to gitlab to run only install and build jobs only.

+ assignment-prep - involves work done for the assignment initial structure.

+ fixtures - The fixtures branch contains data like like login and register credentials used for e2e automation tests.

+ custom-commands - The commands.js file in the custom-commands branch contains the following commands login, logout, checkUrl and checkPageHeader. These commands were used in all e2e tests to minimize code and to automate the authentication process.

+ support-functions - The support-functions branch contains the a support file called "e2e.js". This file contains the following functions: paginateLimit, getTvShowByName, filterTvByRatings, and removeDuplicateCredits. These functions helped me to get the correct comparison data for filtering as well as control the maximum limit of pages that are allowed for the tmdb api.

+ e2e-login - This branch contains the "login.cy.js" test spec for e2etesting. Its functionality is to test the logging in feature on the web app and check for errors while logging in or out.

+ e2e-register - This branch contains the "register.cy.js" test spec for e2etesting. Its functionality is to test the registering feature on the web app and check for error while registering, logging in, and deleting account.

+ e2e-pagination - This branch contains the "pagination.cy.js" test spec for e2etesting. Its functionality is to test all pages with the pagination component and check second and last page of each page and compare its content.

+ e2e-ratings - This branch contains the "ratings.cy.js" test spec for e2etesting. Its functionality is to test the ratings filter and compare content depending on the selected ratings filter.

+ e2e-review - This branch contains the "review.cy.js" test spec for e2etesting. Its functionality is to test the review form and check for errors while writing a review, lastly writting a review in correct format and checking if the review was accepted.

+ e2e-credits - This branch contains the "credits.cy.js" test spec for e2etesting. Its functionality is to test the credits for a selected tv show and compare contents once 'casts' or 'crew' is toggled.

+ e2e-pagination-fix - This branch is responsible for correcting the url for tv pagination.

+ e2e-pagination-fix2 - This branch is responsible for correcting the url for upcoming movies pagination.

+ code-bundling-routes - This branch is responsbile for implementing code-bundling for all routes in the web app, src/index.js.

+ code-splitting - This branch is responsible for code-splitting filtering functionality in all template list pages (src/components/templateTvPage,src/components/templateTvListPage,src/components/templatetvCreditsPage,src/components/templateMovieListPage).

+ assignment-fork-demo, pull-req-1, master - These branches were used to for the source control (Pull request) requirement for the assignment. 

+ component-filter-select-test - This branch was responsible for the implementation of the component test 'creditsfilter.cy.js'.
